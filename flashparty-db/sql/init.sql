CREATE TABLE mobility (
  mobility_cd INT8,
  distance    INT8
);

CREATE TABLE flash_user (
  user_id      BIGSERIAL PRIMARY KEY,
  user_name    TEXT UNIQUE NOT NULL,
  mobility_cd  INT8        NOT NULL,
  location_lon FLOAT8,
  location_lat FLOAT8,
  location_ts  BIGINT
);

CREATE OR REPLACE VIEW user_polygon AS
  SELECT
    user_id,
    st_buffer(st_transform(st_setsrid(st_makepoint(location_lon, location_lat), 4326), 3857), mobility.distance)
      AS the_geom
  FROM flash_user
    JOIN mobility ON flash_user.mobility_cd = mobility.mobility_cd
  WHERE
    mobility IS NOT NULL
    AND location_lon IS NOT NULL
    AND location_lat IS NOT NULL;

CREATE OR REPLACE VIEW user_connection AS
  SELECT
    poly_1.user_id AS initiator_id,
    poly_2.user_id AS receiver_id
  FROM user_polygon poly_1, user_polygon poly_2
  WHERE
    st_overlaps(poly_1.the_geom, poly_2.the_geom)
    AND poly_1.user_id < poly_2.user_id;