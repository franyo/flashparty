INSERT INTO mobility (mobility_cd, distance) VALUES
  (1, 1000),
  (2, 2000),
  (3, 5000),
  (4, 10000);

INSERT INTO flash_user (user_name, mobility_cd, location_lat, location_lon, location_ts) VALUES
  ('John', 1, 47.508050, 19.027197, 1516053469),
  ('Marta', 1, 47.507953, 19.028613, 1516053470),
  ('Zoltan', 1, 47.507452, 19.027717, 1516053471),
  ('Eric', 1, 47.507756, 19.038174, 1516053472),
  ('Kata', 1, 47.507573, 19.066869, 1516053473),
  ('Nate', 1, 47.505355, 19.063639, 1516053473);