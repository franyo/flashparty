package hu.franyo.flash;

import javax.ws.rs.core.Response;
import java.sql.SQLException;

public abstract class BaseService {

    protected interface RequestProcessor {
        Response processRequest(BaseDao dao) throws Exception;
    }

    protected Response handleRequest(RequestProcessor requestProcessor) {
        try (BaseDao dao = createDao()) {
            return requestProcessor.processRequest(dao);
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    protected abstract BaseDao createDao() throws SQLException;
}