package hu.franyo.flash.party;

import java.util.List;

public class FlashPartyDto {

    private List<Long> attendees;

    public FlashPartyDto(List<Long> attendees) {
        this.attendees = attendees;
    }

    public List<Long> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Long> attendees) {
        this.attendees = attendees;
    }
}
