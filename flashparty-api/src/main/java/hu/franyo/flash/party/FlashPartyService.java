package hu.franyo.flash.party;

import com.google.gson.Gson;
import hu.franyo.flash.BaseDao;
import hu.franyo.flash.BaseService;
import hu.franyo.flash.graph.GraphUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@Path("/api/")
public class FlashPartyService extends BaseService {

    @Override
    protected BaseDao createDao() throws SQLException {
        return new FlashPartyDao();
    }

    @GET
    @Path("parties/{lower_limit}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findParties(@PathParam("lower_limit") int lowerLimit) {
        return handleRequest((dao -> {
            List<Long[]> connections = ((FlashPartyDao) dao).findConnections();
            List<List<Long>> maximalCliques = GraphUtils.findMaximalCliques(connections, lowerLimit);
            List<FlashPartyDto> parties = maximalCliques.stream().map(FlashPartyDto::new).collect(Collectors.toList());
            return Response.ok(new Gson().toJson(parties)).build();
        }));
    }
}