package hu.franyo.flash.party;

import hu.franyo.flash.BaseDao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FlashPartyDao extends BaseDao {

    public FlashPartyDao() throws SQLException {
        super();
    }

    public List<Long[]> findConnections() throws SQLException {
        List<Long[]> connections = new ArrayList<>();
        PreparedStatement preparedStatement = connection.prepareStatement(
                "SELECT * FROM user_connection"
        );
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                Long initiatorId = resultSet.getLong("initiator_id");
                Long receiverId = resultSet.getLong("receiver_id");
                connections.add(new Long[]{initiatorId, receiverId});
            }
        }
        return connections;
    }
}