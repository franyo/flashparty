package hu.franyo.flash.user;

import hu.franyo.flash.BaseDao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FlashUserDao extends BaseDao {

    public FlashUserDao() throws SQLException {
        super();
    }

    public FlashUserDto findUser(Long userIdParam) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "SELECT * FROM flash_user WHERE user_id = ?"
        );
        preparedStatement.setLong(1, userIdParam);
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            if (resultSet.next()) {
                return parseUserFromResultSet(resultSet);
            }
        }
        return null;
    }

    public List<FlashUserDto> findUsers() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "SELECT * FROM flash_user"
        );
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            List<FlashUserDto> users = new ArrayList<>();
            while (resultSet.next()) {
                users.add(parseUserFromResultSet(resultSet));
            }
            return users;
        }
    }

    private FlashUserDto parseUserFromResultSet(ResultSet resultSet) throws SQLException {
        long userId = resultSet.getLong("user_id");
        String userName = resultSet.getString("user_name");
        int travelRange = resultSet.getInt("travel_range");
        float locationLon = resultSet.getFloat("location_lon");
        float locationLat = resultSet.getFloat("location_lat");
        long locationTs = resultSet.getLong("location_ts");
        return new FlashUserDto(userId, userName, travelRange, locationLon, locationLat, locationTs);
    }

    public void changeUserLocation(FlashUserDto user) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "UPDATE flash_user SET location_lon = ?, location_lat = ?, location_ts = ? " +
                        "WHERE user_id = ?"
        );
        preparedStatement.setFloat(3, user.getLocationLon());
        preparedStatement.setFloat(4, user.getLocationLat());
        preparedStatement.setLong(3, user.getLocationTs());
        preparedStatement.setLong(4, user.getUserId());
        preparedStatement.executeUpdate();
    }
}