package hu.franyo.flash.user;

import java.io.Serializable;

public class FlashUserDto implements Serializable {

    private long userId;
    private String userName;
    private int travelRange;
    private float locationLon;
    private float locationLat;
    private long locationTs;

    public FlashUserDto() {
    }

    public FlashUserDto(long userId, String userName, int travelRange, float locationLon, float locationLat, long locationTs) {
        this.userId = userId;
        this.userName = userName;
        this.travelRange = travelRange;
        this.locationLon = locationLon;
        this.locationLat = locationLat;
        this.locationTs = locationTs;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getTravelRange() {
        return travelRange;
    }

    public void setTravelRange(int travelRange) {
        this.travelRange = travelRange;
    }

    public float getLocationLon() {
        return locationLon;
    }

    public void setLocationLon(float locationLon) {
        this.locationLon = locationLon;
    }

    public float getLocationLat() {
        return locationLat;
    }

    public void setLocationLat(float locationLat) {
        this.locationLat = locationLat;
    }

    public long getLocationTs() {
        return locationTs;
    }

    public void setLocationTs(long locationTs) {
        this.locationTs = locationTs;
    }
}
