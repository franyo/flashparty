package hu.franyo.flash.user;

import com.google.gson.Gson;
import hu.franyo.flash.BaseDao;
import hu.franyo.flash.BaseService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.List;

@Path("/api/")
public class FlashUserService extends BaseService {

    @Override
    protected BaseDao createDao() throws SQLException {
        return new FlashUserDao();
    }

    @GET
    @Path("user/{user_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findUser(@PathParam("user_id") Long userId) {
        return handleRequest((dao -> {
            FlashUserDto user = ((FlashUserDao) dao).findUser(userId);
            return Response.ok(new Gson().toJson(user)).build();
        }));
    }

    @GET
    @Path("users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findUsers() {
        return handleRequest((dao -> {
            List<FlashUserDto> users = ((FlashUserDao) dao).findUsers();
            return Response.ok(new Gson().toJson(users)).build();
        }));
    }

    @POST
    @Path("user/location")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response changeUserLocation(FlashUserDto user) {
        return handleRequest((dao -> {
            ((FlashUserDao) dao).changeUserLocation(user);
            return Response.ok().build();
        }));
    }
}