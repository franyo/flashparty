package hu.franyo.flash.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class GraphUtils {

    public static List<List<Long>> findMaximalCliques(List<Long[]> connections, int lowerLimit) {
        List<Vertex> vertices = buildVertices(connections);

        List<List<Long>> cliques = new ArrayList<>();
        while (vertices.size() >= lowerLimit) {
            List<Long> attendees = GraphUtils.findMaximalClique(vertices, lowerLimit);
            if (attendees.isEmpty()) {
                vertices.remove(0);
            } else {
                cliques.add(attendees);
                attendees.stream().map(attendee -> new Vertex(null, attendee)).forEach(vertices::remove);
            }
        }
        return cliques;
    }

    private static List<Vertex> buildVertices(List<Long[]> connections) {
        Graph graph = new Graph();
        connections.forEach(connection -> graph.addConnection(connection[0], connection[1]));
        return graph.getValues().stream().map(value -> new Vertex(graph, value)).collect(Collectors.toList());
    }

    private static List<Long> findMaximalClique(List<Vertex> vertices, int lowerLimit) {
        List<Vertex> attendees = new ArrayList<>();
        Set<Vertex> checked = new HashSet<>();
        Set<Vertex> toCheck = new HashSet<>();

        toCheck.add(vertices.get(0));
        while (!toCheck.isEmpty()) {
            List<Vertex> toCheckNext = new ArrayList<>();
            for (Vertex vertex : toCheck) {
                if (checked.contains(vertex)) {
                    continue;
                }

                checked.add(vertex);
                attendees.add(vertex);

                if (isClique(attendees)) {
                    toCheckNext.addAll(vertex.neighbours());
                } else {
                    attendees.remove(vertex);
                }
            }
            toCheck.addAll(toCheckNext);
            toCheck.removeAll(checked);
        }

        if (attendees.size() < lowerLimit) {
            return Collections.emptyList();
        }
        return attendees.stream().map(Vertex::getValue).collect(Collectors.toList());
    }

    private static boolean isClique(List<Vertex> vertices) {
        for (Vertex vertex : vertices) {
            if (!vertex.neighboursAndSelf().containsAll(vertices)) {
                return false;
            }
        }
        return true;
    }
}