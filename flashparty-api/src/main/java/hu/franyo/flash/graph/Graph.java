package hu.franyo.flash.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Graph {

    private List<Long> values;
    private List<List<Long>> connections;

    public Graph() {
        values = new ArrayList<>();
        connections = new ArrayList<>();
    }

    public void addConnection(Long v1, Long v2) {
        if (!values.contains(v1)) {
            values.add(v1);
            connections.add(new ArrayList<>());
        }
        if (!values.contains(v2)) {
            values.add(v2);
            connections.add(new ArrayList<>());
        }

        int i1 = values.indexOf(v1);
        connections.get(i1).add(v2);
        int i2 = values.indexOf(v2);
        connections.get(i2).add(v1);
    }

    public List<Long> neighbours(Long v) {
        if (!values.contains(v)) {
            return Collections.emptyList();
        }
        int i = values.indexOf(v);
        return connections.get(i);
    }

    public List<Long> getValues() {
        return values;
    }
}