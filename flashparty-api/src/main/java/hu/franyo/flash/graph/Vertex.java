package hu.franyo.flash.graph;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Vertex {

    private Graph graph;
    private Long value;

    public Vertex(Graph graph, Long value) {
        this.graph = graph;
        this.value = value;
    }

    public List<Vertex> neighbours() {
        return graph.neighbours(value).stream()
                .map(neighbour -> new Vertex(graph, neighbour))
                .collect(Collectors.toList());
    }

    public List<Vertex> neighboursAndSelf() {
        List<Vertex> neighbours = neighbours();
        neighbours.add(this);
        return neighbours;
    }

    public Long getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vertex vertex = (Vertex) o;
        return Objects.equals(value, vertex.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}