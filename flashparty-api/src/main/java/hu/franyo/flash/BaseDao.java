package hu.franyo.flash;

import org.apache.commons.lang3.StringUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseDao implements AutoCloseable {

    private static final String dbUrl;
    private static final String dbDriver;

    protected final Connection connection;

    static {
        dbUrl = buildDbUrl();
        dbDriver = buildDbDriver();
        try {
            Class.forName(dbDriver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public BaseDao() throws SQLException {
        connection = DriverManager.getConnection(dbUrl);
    }

    private static String buildDbUrl() {
        StringBuilder dbUrlBuilder = new StringBuilder("jdbc:postgresql://");

        String dbHost = System.getenv("DB_HOST");
        dbUrlBuilder.append(StringUtils.isBlank(dbHost) ? "localhost" : dbHost).append(":");

        String dbPort = System.getenv("DB_PORT");
        dbUrlBuilder.append(StringUtils.isBlank(dbPort) ? "5432" : dbPort).append("/");

        String dbName = System.getenv("DB_NAME");
        dbUrlBuilder.append(StringUtils.isBlank(dbName) ? "postgres" : dbName);

        String dbUser = System.getenv("DB_USER");
        dbUrlBuilder.append("?user=").append(StringUtils.isBlank(dbUser) ? "postgres" : dbName);

        String dbPassword = System.getenv("DB_PASSWORD");
        if (!StringUtils.isBlank(dbPassword)) {
            dbUrlBuilder.append("&password=").append(dbPassword);
        }

        return dbUrlBuilder.toString();
    }

    private static String buildDbDriver() {
        String dbDriver = System.getenv("DB_DRIVER");
        return StringUtils.isBlank(dbDriver) ? "org.postgresql.Driver" : dbDriver;
    }

    @Override
    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}