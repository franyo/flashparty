// FLASHPARTY-DEV

docker network create n_flash_party

docker image build flashparty-db/ -t franyo/flashparty-db && \
docker container run -d \
-v v_flash_party_db:/var/lib/postgresql/data \
--name flash_party_db \
--network n_flash_party \
-p 5432:5432 \
franyo/flashparty-db

mvn clean package && java -jar -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 flashparty-api/target/flashparty-api-swarm.jar

mvn clean package && docker image build -t franyo/flashparty-api flashparty-api/
docker container run -d -p 8080:8080 -e DB_HOST=flash_party_db --name flash_party_api --network n_flash_party franyo/flashparty-api